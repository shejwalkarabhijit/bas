<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('isValidYear'))
{
    function isValidYear( $year ) {

        // Convert to timestamp
        $start_year         =   strtotime(date('Y') - 100); //100 Years back
        $received_year      =   strtotime($year);

        // Check that user date is between start & end
        return (($received_year >= $start_year) && ( strlen( $year )  == 4 ) );

    }
}